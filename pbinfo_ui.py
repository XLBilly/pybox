# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'pbinfo_ui.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_PBInfoMain(object):
    def setupUi(self, PBInfoMain):
        if not PBInfoMain.objectName():
            PBInfoMain.setObjectName(u"PBInfoMain")
        PBInfoMain.resize(400, 406)
        icon = QIcon()
        icon.addFile(u"res/pbinfo.ico", QSize(), QIcon.Normal, QIcon.Off)
        PBInfoMain.setWindowIcon(icon)
        self.gridLayout = QGridLayout(PBInfoMain)
        self.gridLayout.setObjectName(u"gridLayout")
        self.tabWidget = QTabWidget(PBInfoMain)
        self.tabWidget.setObjectName(u"tabWidget")
        self.system = QWidget()
        self.system.setObjectName(u"system")
        self.gridLayout_3 = QGridLayout(self.system)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.label_9 = QLabel(self.system)
        self.label_9.setObjectName(u"label_9")
        self.label_9.setPixmap(QPixmap(u"res/mac.svg"))

        self.gridLayout_3.addWidget(self.label_9, 0, 1, 1, 1)

        self.systemName = QLabel(self.system)
        self.systemName.setObjectName(u"systemName")

        self.gridLayout_3.addWidget(self.systemName, 1, 3, 1, 1)

        self.label_10 = QLabel(self.system)
        self.label_10.setObjectName(u"label_10")

        self.gridLayout_3.addWidget(self.label_10, 1, 2, 1, 1)

        self.systemPlatform = QLabel(self.system)
        self.systemPlatform.setObjectName(u"systemPlatform")

        self.gridLayout_3.addWidget(self.systemPlatform, 2, 3, 1, 1)

        self.label = QLabel(self.system)
        self.label.setObjectName(u"label")
        font = QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)

        self.gridLayout_3.addWidget(self.label, 0, 2, 1, 2)

        self.label_3 = QLabel(self.system)
        self.label_3.setObjectName(u"label_3")

        self.gridLayout_3.addWidget(self.label_3, 2, 2, 1, 1)

        self.groupBox = QGroupBox(self.system)
        self.groupBox.setObjectName(u"groupBox")
        self.groupBox.setFlat(False)
        self.gridLayout_6 = QGridLayout(self.groupBox)
        self.gridLayout_6.setObjectName(u"gridLayout_6")
        self.label_20 = QLabel(self.groupBox)
        self.label_20.setObjectName(u"label_20")

        self.gridLayout_6.addWidget(self.label_20, 0, 0, 1, 1)

        self.windowsKernel = QLabel(self.groupBox)
        self.windowsKernel.setObjectName(u"windowsKernel")

        self.gridLayout_6.addWidget(self.windowsKernel, 0, 1, 1, 1)


        self.gridLayout_3.addWidget(self.groupBox, 3, 2, 1, 2)

        self.tabWidget.addTab(self.system, "")
        self.application = QWidget()
        self.application.setObjectName(u"application")
        self.gridLayout_4 = QGridLayout(self.application)
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.label_14 = QLabel(self.application)
        self.label_14.setObjectName(u"label_14")

        self.gridLayout_4.addWidget(self.label_14, 4, 2, 1, 1)

        self.label_7 = QLabel(self.application)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setPixmap(QPixmap(u"res/python.svg"))

        self.gridLayout_4.addWidget(self.label_7, 5, 0, 1, 1)

        self.pythonVerison = QLabel(self.application)
        self.pythonVerison.setObjectName(u"pythonVerison")

        self.gridLayout_4.addWidget(self.pythonVerison, 6, 3, 1, 1)

        self.label_2 = QLabel(self.application)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setTextFormat(Qt.RichText)

        self.gridLayout_4.addWidget(self.label_2, 6, 2, 1, 1)

        self.pythonPrefix = QLabel(self.application)
        self.pythonPrefix.setObjectName(u"pythonPrefix")

        self.gridLayout_4.addWidget(self.pythonPrefix, 7, 3, 1, 1)

        self.label_4 = QLabel(self.application)
        self.label_4.setObjectName(u"label_4")

        self.gridLayout_4.addWidget(self.label_4, 7, 2, 1, 1)

        self.qtVersion = QLabel(self.application)
        self.qtVersion.setObjectName(u"qtVersion")

        self.gridLayout_4.addWidget(self.qtVersion, 3, 3, 1, 1)

        self.pythonExe = QLabel(self.application)
        self.pythonExe.setObjectName(u"pythonExe")

        self.gridLayout_4.addWidget(self.pythonExe, 8, 3, 1, 1)

        self.label_8 = QLabel(self.application)
        self.label_8.setObjectName(u"label_8")
        self.label_8.setStyleSheet(u"font: 75 12pt \"\u5fae\u8f6f\u96c5\u9ed1\";")

        self.gridLayout_4.addWidget(self.label_8, 5, 2, 1, 2)

        self.label_12 = QLabel(self.application)
        self.label_12.setObjectName(u"label_12")
        self.label_12.setFont(font)

        self.gridLayout_4.addWidget(self.label_12, 2, 2, 1, 2)

        self.label_16 = QLabel(self.application)
        self.label_16.setObjectName(u"label_16")
        self.label_16.setPixmap(QPixmap(u"res/info.svg"))

        self.gridLayout_4.addWidget(self.label_16, 0, 0, 1, 1)

        self.pythonArgv = QLabel(self.application)
        self.pythonArgv.setObjectName(u"pythonArgv")

        self.gridLayout_4.addWidget(self.pythonArgv, 9, 3, 1, 1)

        self.label_11 = QLabel(self.application)
        self.label_11.setObjectName(u"label_11")
        self.label_11.setPixmap(QPixmap(u"res/qt.svg"))

        self.gridLayout_4.addWidget(self.label_11, 2, 0, 1, 1)

        self.label_13 = QLabel(self.application)
        self.label_13.setObjectName(u"label_13")

        self.gridLayout_4.addWidget(self.label_13, 3, 2, 1, 1)

        self.pyqtVersion = QLabel(self.application)
        self.pyqtVersion.setObjectName(u"pyqtVersion")

        self.gridLayout_4.addWidget(self.pyqtVersion, 4, 3, 1, 1)

        self.label_17 = QLabel(self.application)
        self.label_17.setObjectName(u"label_17")
        self.label_17.setFont(font)

        self.gridLayout_4.addWidget(self.label_17, 0, 2, 1, 2)

        self.label_6 = QLabel(self.application)
        self.label_6.setObjectName(u"label_6")

        self.gridLayout_4.addWidget(self.label_6, 8, 2, 1, 1)

        self.label_5 = QLabel(self.application)
        self.label_5.setObjectName(u"label_5")

        self.gridLayout_4.addWidget(self.label_5, 9, 2, 1, 1)

        self.label_19 = QLabel(self.application)
        self.label_19.setObjectName(u"label_19")

        self.gridLayout_4.addWidget(self.label_19, 1, 2, 1, 1)

        self.processID = QLabel(self.application)
        self.processID.setObjectName(u"processID")

        self.gridLayout_4.addWidget(self.processID, 1, 3, 1, 1)

        self.tabWidget.addTab(self.application, "")
        self.user = QWidget()
        self.user.setObjectName(u"user")
        self.gridLayout_5 = QGridLayout(self.user)
        self.gridLayout_5.setObjectName(u"gridLayout_5")
        self.label_15 = QLabel(self.user)
        self.label_15.setObjectName(u"label_15")

        self.gridLayout_5.addWidget(self.label_15, 0, 0, 1, 1)

        self.tabWidget.addTab(self.user, "")

        self.gridLayout.addWidget(self.tabWidget, 0, 0, 1, 1)


        self.retranslateUi(PBInfoMain)

        self.tabWidget.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(PBInfoMain)
    # setupUi

    def retranslateUi(self, PBInfoMain):
        PBInfoMain.setWindowTitle(QCoreApplication.translate("PBInfoMain", u"\u4fe1\u606f", None))
        self.label_9.setText("")
        self.systemName.setText(QCoreApplication.translate("PBInfoMain", u"systemName", None))
        self.label_10.setText(QCoreApplication.translate("PBInfoMain", u"<b>\u7cfb\u7edf\u5185\u6838\uff1a</b>", None))
        self.systemPlatform.setText(QCoreApplication.translate("PBInfoMain", u"systemPlatform", None))
        self.label.setText(QCoreApplication.translate("PBInfoMain", u"\u64cd\u4f5c\u7cfb\u7edf", None))
        self.label_3.setText(QCoreApplication.translate("PBInfoMain", u"<b>\u5e73\u53f0\uff1a</b>", None))
        self.groupBox.setTitle(QCoreApplication.translate("PBInfoMain", u"Windows\u4fe1\u606f", None))
        self.label_20.setText(QCoreApplication.translate("PBInfoMain", u"<b>Windows\u5185\u6838\u7248\u672c\uff1a</b>", None))
        self.windowsKernel.setText(QCoreApplication.translate("PBInfoMain", u"windowsKernel", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.system), QCoreApplication.translate("PBInfoMain", u"\u7cfb\u7edf", None))
        self.label_14.setText(QCoreApplication.translate("PBInfoMain", u"<html><head/><body><p><span style=\" font-weight:600;\">PyQt\u7248\u672c</span></p></body></html>", None))
        self.label_7.setText("")
        self.pythonVerison.setText(QCoreApplication.translate("PBInfoMain", u"pythonVersion", None))
        self.label_2.setText(QCoreApplication.translate("PBInfoMain", u"<b>Python\u7248\u672c\uff1a</b>", None))
        self.pythonPrefix.setText(QCoreApplication.translate("PBInfoMain", u"pythonPrefix", None))
        self.label_4.setText(QCoreApplication.translate("PBInfoMain", u"<b>Python\u542f\u52a8\u8def\u5f84\uff1a</b>", None))
        self.qtVersion.setText(QCoreApplication.translate("PBInfoMain", u"qtVersion", None))
        self.pythonExe.setText(QCoreApplication.translate("PBInfoMain", u"pythonExe", None))
        self.label_8.setText(QCoreApplication.translate("PBInfoMain", u"Python\u4fe1\u606f", None))
        self.label_12.setText(QCoreApplication.translate("PBInfoMain", u"Qt\u4fe1\u606f", None))
        self.label_16.setText("")
        self.pythonArgv.setText(QCoreApplication.translate("PBInfoMain", u"pythonArgv", None))
        self.label_11.setText("")
        self.label_13.setText(QCoreApplication.translate("PBInfoMain", u"<b>Qt\u7248\u672c\uff1a</b>", None))
        self.pyqtVersion.setText(QCoreApplication.translate("PBInfoMain", u"pyQtVersion", None))
        self.label_17.setText(QCoreApplication.translate("PBInfoMain", u"\u5e94\u7528\u4fe1\u606f", None))
        self.label_6.setText(QCoreApplication.translate("PBInfoMain", u"<b>Python\u6267\u884c\u6587\u4ef6\u8def\u5f84\uff1a</b>", None))
        self.label_5.setText(QCoreApplication.translate("PBInfoMain", u"<b>Python\u542f\u52a8\u53c2\u6570\uff1a</b>", None))
        self.label_19.setText(QCoreApplication.translate("PBInfoMain", u"<b>\u5e94\u7528PID\uff1a</b>", None))
        self.processID.setText(QCoreApplication.translate("PBInfoMain", u"processID", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.application), QCoreApplication.translate("PBInfoMain", u"\u5e94\u7528\u7a0b\u5e8f", None))
        self.label_15.setText(QCoreApplication.translate("PBInfoMain", u"TextLabel", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.user), QCoreApplication.translate("PBInfoMain", u"\u7528\u6237", None))
    # retranslateUi

