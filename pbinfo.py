import sys, os
from pbinfo_ui import Ui_PBInfoMain
from PySide2 import QtWidgets, QtCore, QtGui


class PBInfo(QtWidgets.QWidget, Ui_PBInfoMain):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.retranslateUi(self)
        self.updateInfo()
        if sys.platform != 'win32' or sys.platform != 'cygwin':
            self.gridLayout_3.removeWidget(self.groupBox)
            self.groupBox.deleteLater()

    def updateInfo(self):
        """Update Information"""
        self.systemPlatform.setText(sys.platform)
        self.systemName.setText(os.name)
        if sys.platform == 'win32' or sys.platform == 'cygwin':
            self.label_9.setPixmap(QtGui.QPixmap(u"res/windows.svg"))
            self.windowsKernel.setText(sys.winver)
        if sys.platform == 'linux':
            self.label_9.setPixmap(QtGui.QPixmap(u"res/linux.svg"))
        self.pythonVerison.setText(sys.version)
        self.pythonPrefix.setText(sys.prefix)
        self.pythonExe.setText(sys.executable)
        self.qtVersion.setText(QtCore.__version__)
        self.processID.setText(str(os.getppid()))


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    info = PBInfo()
    info.show()
    sys.exit(app.exec_())
